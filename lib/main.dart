
import 'package:classroom_project/data/classes.dart';
import 'package:classroom_project/home.dart';

import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'login.dart';
import 'home.dart';



Future<void> main() async{
  WidgetsFlutterBinding.ensureInitialized();

  await Firebase.initializeApp();


  runApp(MaterialApp(
    routes: {
      '/home' : (context) => Home(),
      '/login': (context) => Login(),
    },
    home:Login(),
  ));
}
