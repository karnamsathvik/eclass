import 'package:classroom_project/data/CourseView.dart';
import 'package:classroom_project/signIn/GoogleSignInProvider.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'variables.dart';
import 'package:flutter/cupertino.dart';


class Home extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return  Scaffold(
      floatingActionButton: FloatingActionButton(
        child: Icon(
            Icons.add
        ),
        onPressed: () {
          showDialog<String>(context: context,
              builder: (BuildContext context) => SimpleDialog(
                children: [
                  ListTile(
                    leading: Icon(Icons.add),
                    title: Text('Create a class'),
                    // TODO : NAVIGATE TO CREATE A CLASS
                    onTap: () {
                      mydialogue(context,'enter subject');
                    },
                  ),
                  ListTile(
                    leading: Icon(Icons.add),
                    title: Text('Join a class'),
                    // TODO : NAVIGATE TO JOIN A CLASS
                    onTap: () {
                        mydialogue(context, 'Join a class');
                    }
                  )
                  ],
              )
          );
        },
      ),
        appBar: AppBar(
          backgroundColor: Colors.lightBlueAccent,
          leading: Padding(
            padding: const EdgeInsets.all(6.0),
            child: CircleAvatar(
              radius: 2.0,
              backgroundImage: NetworkImage(FirebaseAuth.instance.currentUser.photoURL),
            ),
          ),
          title: Text("e-Classroom"),
          centerTitle: true,
          actions: [
            IconButton(icon: Icon(Icons.logout), onPressed:() async {
              final provider = Provider.of<GoogleSignInProvider>(context,listen: false);
              provider.logout();
            })
          ],
        ),
        body: CourseList(),
      );
  }
}