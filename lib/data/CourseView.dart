import 'dart:math';

import 'package:classroom_project/data/classes.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';

class CourseList extends StatefulWidget {

  @override
  _CourseListState createState() => _CourseListState();
}

class _CourseListState extends State<CourseList> {
  
  final _firestore = FirebaseFirestore.instance;
  
  @override
  Widget build(BuildContext context) {
    return StreamBuilder(
      stream: _firestore.collection('teacher').snapshots(),
      builder: (BuildContext context, AsyncSnapshot<QuerySnapshot<Map<String, dynamic>>> teacher) {
        if(teacher.hasData){
          return StreamBuilder(
            stream: _firestore.collection('course').snapshots(),
            builder: (BuildContext context, AsyncSnapshot<QuerySnapshot<Map<String, dynamic>>> course) {
              if(course.hasData){
                return StreamBuilder(
                  stream: FirebaseFirestore.instance.collection('student').snapshots(),
                  builder: (BuildContext context, AsyncSnapshot<QuerySnapshot<Map<String, dynamic>>> student) {
                    if(student.hasData){
                      final teachers= teacher.data.docs;
                      final courses = course.data.docs;
                      final students = student.data.docs;
                      List<Widget> mycourses =[];
                      List<Widget> teaching = [];
                      for( var t in teachers){
                        if(t.get('email')==FirebaseAuth.instance.currentUser.email){
                          for( var c in courses){
                            if(t.get('courseid')==c.get('courseid')){
                              mycourses.add(CourseTile(c.get('subject'),c.get('courseid'),t.get('email')));
                            }
                          }
                        }
                      }
                      for( var s in students){
                        if(s.get('email')==FirebaseAuth.instance.currentUser.email){
                          for( var c in courses){
                            if(s.get('courseid')==c.get('courseid')){
                              for( var t in teachers){
                                if(t.get('courseid')==s.get('courseid')){
                                  teaching.add(CourseTile(c.get('subject'),c.get('courseid'),t.get('email')));
                                }
                              }
                            }
                          }
                        }
                      }
                      return SingleChildScrollView(
                        child: Column(
                          children: [
                            Text('My Courses',style: TextStyle(fontSize: 30,fontStyle: FontStyle.italic),),
                            Column(children: mycourses,),
                            Text("Teaching",style: TextStyle(fontSize: 30,fontStyle: FontStyle.italic),),
                            Column(children: teaching,)
                          ],
                        ),
                      );
                    }
                    return Container();
                  },
                );
              }
              return Container();
            },);
        }
        return Container();
      },

    );
  }

  Widget CourseTile(String courseName,int id,String t){

    var v = [Colors.redAccent,Colors.blue,Colors.greenAccent,Colors.orange,Colors.amberAccent,Colors.cyan,Colors.lightBlueAccent,Colors.cyanAccent];
    var r = new Random();
    return Card(
      child: Container(
        height: 100,
        decoration: BoxDecoration(
            gradient: LinearGradient(colors: [
              v[r.nextInt(v.length)],v[r.nextInt(v.length)]
            ])
        ),
        child: Center(
          child: ListTile(
            leading: Padding(
              padding: const EdgeInsets.all(8.0),
              child: Icon(Icons.person_pin_sharp,size: 50,),
            ),
            title: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                Text(courseName.toUpperCase(),style: TextStyle(fontSize: 30),),
                Text(t,style: TextStyle(fontStyle: FontStyle.italic),)
              ],
            ),
            onTap: (){
              Navigator.push(context, MaterialPageRoute(builder: (context)=> Classes(id)));
            },
          ),
        ),
      ),
    );
  }
  
}

