import 'package:classroom_project/home.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'dart:async';
import 'package:url_launcher/url_launcher.dart';

class Classes extends StatefulWidget {
  int id;

  Classes(this.id);

  @override
  _ClassesState createState() => _ClassesState(this.id);
}

class _ClassesState extends State<Classes> {
  int id;
  Future<void> _launched;
  String _launchUrl = 'https://docs.google.com/forms/u/0/';

  Future<void> _launchInBrowser(String url) async {
    if (await canLaunch(url)) {
      await launch(
        url,
        forceSafariVC: false,
        forceWebView: false,
        headers: <String, String>{'header_key': 'header_value'},
      );
    } else {
      throw 'Could not launch $url';
    }
  }

  _ClassesState(this.id);

  @override
  Widget build(BuildContext context) {
    final List<Widget> stud = [];
    Widget tech,sub ;
    return SafeArea(
      child: Scaffold(
          body: StreamBuilder(
            stream: FirebaseFirestore.instance.collection('course').snapshots(),
            builder: (BuildContext context, AsyncSnapshot<QuerySnapshot<Map<String, dynamic>>> course) {
              if(course.hasData){
                return StreamBuilder(
                  stream: FirebaseFirestore.instance.collection('teacher').snapshots(),
                  builder: (BuildContext context, AsyncSnapshot<QuerySnapshot<Map<String, dynamic>>> teacher) {
                    if(teacher.hasData){
                      return StreamBuilder(
                        stream: FirebaseFirestore.instance.collection('student').snapshots(),
                        builder: (BuildContext context, AsyncSnapshot<QuerySnapshot<Map<String, dynamic>>> student) {
                          if(student.hasData){
                            final courses = course.data.docs;
                            final teachers = teacher.data.docs;
                            final students = student.data.docs;
                            for(var c in courses){
                              if(c.get('courseid')==id){
                                sub=Text(c.get('subject').toString().toUpperCase());
                                for(var s in students){
                                  if(s.get('courseid')==c.get('courseid')){
                                    stud.add(studentView(s.get('email')));
                                  }
                                }
                              }
                            }
                            for(var t in teachers){
                              if(t.get('courseid')==id){
                                tech=studentView(t.get('email'));
                              }
                            }
                            return NestedScrollView(
                              headerSliverBuilder: (BuildContext context, bool innerBoxIsScrolled) {
                                return <Widget>[
                                  SliverAppBar(
                                    pinned: true,
                                    expandedHeight: 250.0,
                                    flexibleSpace: FlexibleSpaceBar(
                                      title: Row(
                                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                        children: [
                                          sub,
                                          MaterialButton(
                                              color: Colors.greenAccent,
                                              onPressed: (){
                                                setState(() {
                                                  _launched = _launchInBrowser(_launchUrl);
                                                });
                                              }, child: Text("Assignment"))
                                        ],
                                      ),
                                    ),
                                  ),
                                ];
                              },
                              body: SingleChildScrollView(
                                child: Column(
                                  children: [
                                    Text("Courseid: $id",style: TextStyle(fontSize: 30),),
                                    Text("Teacher:",style: TextStyle(fontSize: 30),),
                                    tech,
                                    Text("Students:",style: TextStyle(fontSize: 30),),
                                    Column(children: stud,)
                                  ],
                                ),
                              ),
                            );
                          }
                          return Container();
                        },
                      );
                    }
                    return Container();
                  },
                );
              }
              return Container();
              },
      )),
    );
  }

  Widget studentView(String email){
    return Card(
      color: Colors.white70,
      child: Center(child: Text(email)),
    );
  }
}

