import 'package:classroom_project/data/classes.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

final str1 = TextEditingController();

Future<dynamic> mydialogue(
  BuildContext context,
  String str,
) async {
  int s;
  return showDialog(
      context: context,
      builder: (BuildContext context) => SimpleDialog(
            title: Text('Enter'),
            children: [
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: TextField(
                  controller: str1,
                  decoration: InputDecoration(
                    labelText: '$str',
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: RaisedButton(
                    child: Text('submit'),
                    onPressed: () {
                      if (str1.text.isNotEmpty) {
                        if (str == 'Join a class') {
                          FirebaseFirestore.instance.collection('student').add({
                            'courseid': int.parse(str1.text),
                            'email': FirebaseAuth.instance.currentUser.email
                                .toString()
                          });
                          var l = StreamBuilder(
                            stream: FirebaseFirestore.instance
                                .collection('course')
                                .snapshots(),
                            builder: (BuildContext context,
                                AsyncSnapshot<dynamic> snapshot) {
                              if (snapshot.hasData) {
                                final courses = snapshot.data.docs;
                                for (var c in courses) {
                                  if (int.parse(str1.text) ==
                                      c.get('courseid')) {
                                    FirebaseFirestore.instance
                                        .collection('course')
                                        .doc(c.id)
                                        .update({
                                      'students': c.get('students') + 1
                                    });
                                  }
                                }
                                return Classes(int.parse(str1.text));
                              }
                              return Container();
                            },
                          );
                        } else {
                          FirebaseDatabase.instance
                              .reference()
                              .child('/count')
                              .once()
                              .then((value) {
                            s = value.value['count'];
                            print(s);
                            FirebaseFirestore.instance
                                .collection('course')
                                .add({
                              'courseid': s + 1,
                              'subject': str1.text.toString(),
                              'students': 0
                            });
                            FirebaseFirestore.instance
                                .collection('teacher')
                                .add({
                              'courseid': s + 1,
                              'email': FirebaseAuth.instance.currentUser.email
                                  .toString()
                            });
                            FirebaseDatabase.instance
                                .reference()
                                .child('/count')
                                .update({'count': s + 1});
                          });
                        }
                        Navigator.pop(context);
                        Navigator.pop(context);
                      }
                    }),
              ),
            ],
          ));
}
